# Budas, yogis y otros

<div style="text-align:center"><img src="activos/imagenes/prahlad-jani-624x385.jpg" width="318" height="196" /></div>

Prahlad Jani fue un hombre indio que afirmaba haber vivido sin comida ni agua durante más de 80 años. Jani, también conocido como "Mataji" o "Chhoti Mataji", ha sido objeto de estudio por parte de médicos y científicos que han intentado comprender cómo es posible que haya sobrevivido sin comida ni agua durante tanto tiempo.

En 2003 y 2010, Jani fue sometido a estudios médicos en un hospital de Ahmedabad, India, donde los médicos le monitorizaron durante varios días sin comida ni agua. Los resultados de los estudios fueron controvertidos y no se ha llegado a una conclusión definitiva sobre cómo es posible que haya sobrevivido sin comida ni agua.

Aunque Prahlad Jani fue un personaje conocido en India, su historia sigue siendo objeto de debate y es posible que no sea cierto que haya sobrevivido sin comida ni agua durante tanto tiempo. Por lo tanto, es importante mantener una mente abierta y no tomar sus afirmaciones como verdades absolutas sin una verificación exhaustiva y rigurosa.

## ¿Qué es un *"buda"*?

<div style="text-align:center"><img src="activos/imagenes/siddhartha-gautama-417x720.png" width="212" height="366" /></div>

"Buda" es un término que se refiere a un ser iluminado en el budismo, una religión y filosofía originaria de la India. El término "Buda" proviene de la palabra sánscrita "budh", que significa "despertar" o "iluminación".

En el budismo, un Buda es alguien que ha alcanzado la iluminación espiritual y ha comprendido la verdad profunda del universo y de la existencia humana. Un Buda ha superado la ignorancia, el sufrimiento y la condición cíclica de la vida y la muerte, y ha alcanzado un estado de conciencia ilimitado y paz interior.

Hay varios Budas importantes en el budismo, incluyendo a Siddhartha Gautama, quien es conocido como el Buda histórico o el Buda Shakyamuni. Siddhartha Gautama vivió en la India hace más de 2,500 años y es considerado el fundador del budismo.

En el budismo, se cree que cualquier persona puede alcanzar la iluminación y convertirse en un Buda, siguiendo los enseñamientos y prácticas budistas. La búsqueda de la iluminación es vista como un camino personal y espiritual, y se basa en la comprensión de la verdad y la práctica de la sabiduría, la compasión y la disciplina moral.

En el budismo, hay varios Budas importantes, cada uno con su propia historia y enseñanzas. Aquí hay algunos de los Budas más importantes:

- Siddhartha Gautama: es conocido como el Buda histórico o el Buda Shakyamuni, y es considerado el fundador del budismo. Nació en la India hace más de 2,500 años y alcanzó la iluminación bajo un árbol de bodhi.

- Amitabha: es uno de los budas de la tradición Mahayana, y es conocido como el buda de la luz infinita y la compasión. Se cree que Amitabha reside en el "Puro Tierra Pura" y es responsable de guiar a los seres hacia su reino después de la muerte.

- Maitreya: es el buda futuro que se espera que alcance la iluminación y reine sobre el mundo en una era futura. Se cree que Maitreya está actualmente en el "Tushita Cielo" esperando su momento para descender a la tierra y guiar a los seres hacia la liberación.

- Padmasambhava: es un Buda importante en el budismo tibetano, y es considerado el fundador del budismo en el Tibet. Conocido como el "Maestro de la Magia", Padmasambhava es conocido por su capacidad para vencer a los demonios y proteger la enseñanza budista.

- Akshobhya: es uno de los cinco budas de la tradición Mahayana, y es conocido como el buda de la Imperturbabilidad. Se dice que Akshobhya simboliza la transformación de la ira y la hostilidad en paz y equilibrio.

Estos son solo algunos de los Budas más importantes en el budismo, y hay muchos otros en diferentes tradiciones y escuelas budistas. Cada uno de estos budas representa cualidades específicas y enseñanzas únicas, y es objeto de veneración y estudio por parte de los seguidores budistas.

En el budismo, no se reconoce a ninguna persona como un Buda en la actualidad. Se cree que un Buda es alguien que ha alcanzado la iluminación y la liberación espiritual, y que vive en un estado permanente de sabiduría y compasión.

En la tradición budista, se considera que el último Buda histórico fue Siddhartha Gautama, el Buda Shakyamuni, quien vivió hace más de 2,500 años. Se espera que haya un próximo Buda, Maitreya, que alcance la iluminación y reine sobre el mundo en una era futura.

Hay muchas personas que practican el budismo y siguen sus enseñanzas, pero no se considera que ninguna de ellas sea un Buda en el sentido tradicional. Sin embargo, se reconoce a algunos maestros budistas y líderes espirituales como tulkus, rinpoches y lamas, que son considerados reencarnaciones de antiguos maestros budistas y son objeto de gran respeto y veneración en algunas tradiciones budistas.

## ¿Qué es un *"yogi"*?

<div style="text-align:center"><img src="activos/imagenes/paramahansa-yogananda-1024x1280.jpg" width="317" height="396" /></div>

"Yogi" es un término utilizado en el hinduismo y el yoga para referirse a una persona que ha alcanzado un estado elevado de conciencia y una conexión profunda con lo divino. Un yogui es alguien que ha cultivado la práctica de yoga y meditación para lograr un estado de equilibrio y armonía en su vida y en su relación con el universo.

El término "yogi" también se utiliza para describir a alguien que sigue un estilo de vida ascético y dedicado a la práctica espiritual. Los yoguis pueden retirarse a la soledad y la meditación por largos períodos de tiempo, y pueden adoptar prácticas como la abstinencia de comida, agua y otros placeres materiales para alcanzar un estado de purificación y elevación espiritual.

Es importante destacar que el yoga y el estado de "yogi" no están limitados a ninguna religión o cultura en particular, y que la práctica del yoga se ha expandido en todo el mundo como una forma de mejorar la salud física, mental y emocional.

Hay muchos yoguis conocidos en la historia y en la actualidad. Algunos de los yoguis más conocidos incluyen:

- Patanjali: Patanjali es conocido como el padre del yoga y es el autor de los Yoga Sutras, uno de los textos más antiguos y respetados sobre el yoga.

- Maharishi Mahesh Yogi: Maharishi Mahesh Yogi es conocido como el fundador de la Transcendental Meditation, una técnica de meditación popular en todo el mundo.

- Paramahansa Yogananda: Paramahansa Yogananda es conocido como el autor de "Autobiografía de un Yogui", un libro que ha sido leído por millones de personas en todo el mundo.

- B.K.S. Iyengar: B.K.S. Iyengar es uno de los yoguis más conocidos y respetados de la India, y es conocido como el padre del yoga moderno.

- Sri Sri Ravi Shankar: Sri Sri Ravi Shankar es un yogui y líder espiritual indio conocido por su trabajo en la promoción de la paz y la comprensión intercultural.

Estos son solo algunos de los yoguis más conocidos, y hay muchos otros que han dejado un impacto significativo en la historia y la cultura del yoga. Es importante destacar que el yoga no depende de la adoración a una figura o gurú en particular, y que la práctica se basa en la búsqueda personal y la autodisciplina.

## ¿Qué es un *"gurú"*?

<div style="text-align:center"><img src="activos/imagenes/sadhguru-316x546.png" width="316" height="546" /></div>

En el hinduismo, el sijismo, el jainismo y otras tradiciones espirituales de la India, un gurú es un maestro espiritual, un guía y mentor que enseña la verdad espiritual y ayuda a sus discípulos a avanzar en su camino hacia la iluminación o la liberación. El término "gurú" proviene del sánscrito y significa "maestro" o "guía".

El papel del gurú es muy importante en la tradición espiritual de la India, y es visto como alguien que tiene una comprensión profunda y directa de la verdad espiritual y puede ayudar a otros a alcanzar esa comprensión. El gurú es considerado como un intermediario entre el estudiante y la verdad espiritual, y se espera que tenga un conocimiento y una comprensión profundos de los textos sagrados, las prácticas espirituales y la experiencia directa de la verdad espiritual.

El discipulado bajo un gurú es visto como una relación íntima y personal, en la que el discípulo aprende de su maestro y recibe su orientación y enseñanza en una variedad de áreas, incluyendo la meditación, la ética, la teología, la filosofía y la práctica de la vida cotidiana. El gurú también puede guiar al discípulo en su camino espiritual y ayudarlo a superar los obstáculos y desafíos que puedan surgir.

Hay muchos gurús conocidos y venerados en la India y en todo el mundo. Algunos de los gurús más conocidos incluyen:

- Sri Ramana Maharshi: Un gurú indio del siglo XX, Ramana Maharshi es conocido por su enseñanza del "autoindagación", una técnica de meditación que implica la investigación directa de la naturaleza de uno mismo.

- Paramahansa Yogananda: Un gurú indio del siglo XX que fundó la organización espiritual "Self-Realization Fellowship", Yogananda es conocido por su enseñanza de la "ciencia de la meditación" y su énfasis en la unión con Dios a través de la práctica espiritual.

- Swami Vivekananda: Un gurú indio del siglo XIX que fue uno de los primeros maestros espirituales de la India en llevar su mensaje al mundo occidental, Vivekananda es conocido por su énfasis en la unidad y la igualdad de todas las religiones.

- Amma (Mata Amritanandamayi): Una gurú india contemporánea conocida por su labor humanitaria y su enseñanza del amor y la compasión.

- Osho (Bhagwan Shree Rajneesh): Un gurú indio del siglo XX que fundó una comunidad espiritual en Oregon, Estados Unidos, Osho es conocido por su enseñanza del "amor libre" y su énfasis en la meditación y la autoindagación.

Estos son solo algunos de los muchos gurús reconocidos en la India y en todo el mundo. Cada uno de ellos tiene su propia enseñanza y enfoque espiritual, y cada uno ha sido influyente en la vida de sus discípulos y seguidores.

## ¿Existe una relación entre “un buda” , “un yogi” y "gurú"?

Sí, existe una relación entre "un buda", "un yogi" y "un gurú" en el contexto de las tradiciones espirituales de la India. Por lo general, hay una relación entre un Buda y un yogui. El yoga es una tradición filosófica y práctica originaria de la India que se remonta a miles de años antes del nacimiento del budismo.

Como se ha mencionado anteriormente, un buda es alguien que ha alcanzado la iluminación o el despertar espiritual completo, liberándose del sufrimiento y del ciclo de nacimiento y muerte (samsara). En el budismo, Siddhartha Gautama, el Buda histórico, se considera que era un yogui antes de alcanzar la iluminación. Se dice que practicó diferentes formas de yoga y meditación en su búsqueda de la verdad y la liberación espiritual.

Por otro lado, un yogi es alguien que practica el yoga, una disciplina espiritual que incluye prácticas de meditación, respiración, posturas físicas y otras técnicas para alcanzar el bienestar y la unión con lo divino. Los yogis son comúnmente considerados maestros espirituales y guías para aquellos que buscan la iluminación y la liberación espiritual.

Desde entonces, muchas tradiciones budistas han incorporado prácticas y enseñanzas del yoga en sus propias prácticas y filosofías. De hecho, algunas tradiciones budistas, como el budismo tibetano, incluyen prácticas y enseñanzas similares a las del yoga en sus ritos y tradiciones.

En general, ambos el yoga y el budismo buscan la autorrealización y la liberación espiritual, y comparten algunas prácticas y enseñanzas similares, como la meditación, la atención plena y la compasión. Sin embargo, también existen diferencias significativas entre las dos tradiciones, y cada una tiene sus propios objetivos, enfoques y enseñanzas únicas.

Como se ha expuesto previamente, un gurú es un maestro espiritual que guía a sus discípulos en su camino hacia la iluminación y la realización de su verdadera naturaleza divina. En las tradiciones espirituales de la India, los gurús son altamente venerados y considerados como la fuente de la sabiduría espiritual y la verdad.

Aunque estos términos representan diferentes aspectos de la espiritualidad y la iluminación, están interconectados en las tradiciones espirituales de la India. Un yogi puede ser un gurú que guía a sus estudiantes en la práctica del yoga, y un buda puede ser visto como un maestro espiritual que ha alcanzado la iluminación y la liberación completa. Además, un gurú puede practicar el yoga y ayudar a sus discípulos a alcanzar la liberación espiritual, mientras que un yogi puede ser un maestro espiritual que guía a sus estudiantes hacia la iluminación.

## ¿Existe algo similar a "un buda" de occidente?

En el Occidente, no existe un concepto exactamente equivalente al de un Buda en el budismo. Sin embargo, hay muchas tradiciones espirituales y filosóficas que comparten ciertos aspectos con el budismo, y que ofrecen enseñanzas y prácticas similares para alcanzar la iluminación y la liberación espiritual.

Algunos ejemplos de tradiciones espirituales occidentales que comparten ciertos aspectos con el budismo incluyen:

- El cristianismo místico: incluye tradiciones como el cristianismo contemplativo, que enfatiza la experiencia personal de Dios y la unión con la divinidad.

- La filosofía neoplátonica: que busca una comprensión profunda de la naturaleza del universo y de la conexión entre el individuo y el cosmos.

- La filosofía panteísta: que sostiene que Dios y el universo son la misma cosa y que el objetivo de la vida es alcanzar una unión con la divinidad.

- El gnosticismo: que enseña que el objetivo de la vida es adquirir conocimiento y comprensión espirituales para alcanzar la liberación del cuerpo y la materia.

Estos son solo algunos ejemplos de tradiciones espirituales y filosóficas occidentales que comparten ciertos aspectos con el budismo, y cada una ofrece enseñanzas y prácticas únicas para alcanzar un estado de iluminación y liberación espiritual. Sin embargo, no hay un concepto exactamente equivalente al de un Buda en el budismo en la cultura occidental.

### La filosofía neoplátonica

<div style="text-align:center"><img src="activos/imagenes/plotino-1143x1600.jpg" width="422" height="590" /></div>

La filosofía neoplatonista es una tradición filosófica que se desarrolló en el Imperio Romano y la antigua Grecia. Algunas de las personas más reconocidas en esta tradición incluyen:

- Plotino: Fundador del neoplatonismo, Plotino es conocido por su obra "Las Enéadas", en la que expone su teoría del universo y la naturaleza del ser humano.

- Proclo: Uno de los más grandes comentaristas de la obra de Platón, Proclo es conocido por su obra "El Teologium", en la que explora la naturaleza de Dios y el universo.

- Jamblico: Un filósofo y teólogo neoplatonista, Jamblico es conocido por su obra "El tetralogium", en la que explora la naturaleza de la realidad y la verdad.

- Damascius: El último de los filósofos neoplatonistas, Damascius es conocido por su obra "Las Vidas y Opiniones de los Filósofos Ilustres", una historia de la filosofía antigua.

Estos son solo algunos ejemplos de las personas más reconocidas en la tradición filosófica neoplatonista. La neoplatonismo tuvo un impacto profundo en la filosofía y la teología occidentales y sigue siendo estudiado y debatido en la actualidad.

En la actualidad, no hay una tradición neoplatonista viva en el sentido literal, pero la influencia del neoplatonismo sigue siendo evidente en diversos campos, incluyendo la filosofía, la teología, el arte y la literatura. Muchos filósofos contemporáneos continúan explorando y desarrollando ideas neoplatonistas, y el neoplatonismo sigue siendo una influencia importante en el pensamiento occidental.

Hay algunos filósofos y pensadores contemporáneos que han sido influenciados por el neoplatonismo y han explorado temas similares a los abordados por los antiguos filósofos neoplatonistas. Sin embargo, es difícil señalar a una persona específica como líder de una tradición neoplatonista contemporánea.

### El cristianismo místico

<div style="text-align:center"><img src="activos/imagenes/meister-eckhart-374x463.png" width="374" height="463" /></div>

El cristianismo místico es una tradición espiritual dentro del cristianismo que se centra en la experiencia directa de Dios y en la unión con la divinidad. Algunos de los místicos cristianos más reconocidos incluyen:

- Santa Teresa de Ávila: Una mística y reformadora espiritual española del siglo XVI, es conocida por sus escritos sobre su experiencia de unión con Dios y por ser una de las fundadoras de la orden de las Carmelitas Descalzas.

- San Juan de la Cruz: Un místico y poeta español del siglo XVI, es conocido por sus escritos sobre la unión con Dios y por ser uno de los fundadores de la orden de los Carmelitas Descalzas.

- Meister Eckhart: Un místico alemán y teólogo del siglo XIV, es conocido por sus enseñanzas sobre la unión con Dios y por ser uno de los más grandes místicos cristianos de la Edad Media.

- Julian of Norwich: Una mística inglesa del siglo XIV, es conocida por sus escritos sobre su experiencia de unión con Dios y por ser una de las primeras escritoras cristianas en inglés.

- Hildegard of Bingen: Una mística, autora y compositora alemana del siglo XII, es conocida por sus escritos sobre su experiencia de unión con Dios y por ser una de las primeras mujeres en escribir música y teología en la Edad Media.

Estos son solo algunos ejemplos de místicos cristianos reconocidos, y cada uno de ellos ha dejado un legado significativo en la tradición cristiana mística y en la historia de la espiritualidad cristiana en general. También hay muchos místicos cristianos que son reconocidos y respetados en la actualidad. Aquí hay algunos ejemplos:

- Thomas Merton: Un monje trappista estadounidense, es conocido por sus escritos sobre la espiritualidad y la contemplación, así como por su compromiso con la paz y la justicia social.

- Henri Nouwen: Un escritor y teólogo holandés-canadiense, es conocido por sus escritos sobre la espiritualidad y la vida interior, así como por su trabajo como activista y defensor de los marginados y los pobres.

- Bede Griffiths: Un monje inglés, es conocido por su trabajo en la fusión de la espiritualidad cristiana y hindú, así como por sus escritos sobre la contemplación y la unión con Dios.

- Cynthia Bourgeault: Una escritora y maestra espiritual estadounidense, es conocida por sus enseñanzas sobre la espiritualidad contemplativa y la unión con Dios, así como por su trabajo como líder de retiros espirituales.

- Richard Rohr: Un fraile franciscano estadounidense, es conocido por sus enseñanzas sobre la espiritualidad cristiana y el desarrollo del alma, así como por su trabajo como líder de retiros espirituales y conferencista.

### La filosofía panteísta

<div style="text-align:center"><img src="activos/imagenes/einstein-albert-1200x900.jpg" width="444" height="333" /></div>

La filosofía panteísta sostiene que Dios o el universo es equivalente a la realidad material y no dualista, y que el ser humano y el universo son un todo. Algunos de los filósofos y pensadores panteístas más reconocidos incluyen:

- Baruch Spinoza: Un filósofo holandés del siglo XVII, Spinoza es conocido por su obra "Ética Demostrada Según el Orden Geométrico", en la que presenta una visión panteísta de Dios y el universo.

- Georg Wilhelm Friedrich Hegel: Un filósofo alemán del siglo XIX, Hegel es conocido por su teoría del desarrollo histórico y su visión panteísta del universo como una totalidad en constante evolución.

- Albert Einstein: Físico teórico y líder en la revolución científica del siglo XX, Einstein es conocido por haber desarrollado una visión panteísta del universo basada en su comprensión de la teoría de la relatividad.

- John Dewey: Filósofo y educador estadounidense, Dewey es conocido por su teoría del pragmatismo y su visión panteísta de la realidad como un todo dinámico e interdependiente.

Estos son solo algunos ejemplos de los filósofos y pensadores panteístas más reconocidos. La filosofía panteísta ha sido influyente en diversos campos, incluyendo la filosofía, la religión, la ciencia y el humanismo, y sigue siendo debatida y explorada en la actualidad.

Aunque hay pocos filósofos reconocidos que se identifiquen explícitamente como panteístas en la actualidad, hay algunos pensadores que han desarrollado ideas similares o que han influido en el pensamiento panteísta. Algunos ejemplos incluyen:

- Ken Wilber: filósofo y teólogo integral, es conocido por su teoría del "holomovement" que aboga por una visión integral del universo como un todo unitario.

- Terence McKenna: filósofo, escritor y orador, es conocido por su teoría del "árbol del conocimiento" que aboga por una visión holística del universo.

- Rupert Sheldrake: biólogo y filósofo de la ciencia, es conocido por su teoría de la "morfogénesis" que sostiene que la forma y el patrón en el universo provienen de una fuente unitaria.

- Deepak Chopra: médico y filósofo espiritual, es conocido por su enfoque en la conexión entre la mente y el cuerpo, y su defensa de una visión panteísta del universo.

Estos son solo algunos ejemplos de personas reconocidas que han influido en el pensamiento panteísta, pero hay muchos otros pensadores y filósofos que han contribuido a esta tradición a lo largo de la historia.

### El gnosticismo

<div style="text-align:center"><img src="activos/imagenes/carl-jung-797x498.png" width="406" height="253" /></div>

El gnosticismo es una corriente filosófica y religiosa que sostiene que el conocimiento es el medio para la salvación, y que la materia es inferior al espíritu. Aunque el gnosticismo fue popular en la antigüedad, no es un movimiento religioso organizado en la actualidad y por lo tanto no hay muchas personas reconocidas como gnósticos en el sentido estricto. Sin embargo, hay algunos pensadores y escritores contemporáneos que han sido influenciados por las ideas gnósticas o que han desarrollado ideas similares. Algunos ejemplos incluyen:

- Philip K. Dick: escritor de ciencia ficción, es conocido por sus obras que exploran temas gnósticos como la naturaleza de la realidad y la existencia de una inteligencia superior.

- Carl Jung: psiquiatra y psicólogo, es conocido por su teoría de la psicología profunda, que incluye elementos gnósticos como la importancia del conocimiento interior y la búsqueda de la verdad.

- Jordan Peterson: psicólogo y escritor, es conocido por su defensa de la importancia del conocimiento y la verdad, y por su crítica de la cultura postmoderna, que él ve como una forma de gnosticismo moderno.

Estos son solo algunos ejemplos de personas que han sido influenciadas por las ideas gnósticas o que han desarrollado ideas similares. Sin embargo, es importante señalar que no todas las ideas o teorías de estas personas son necesariamente gnósticas, y que cada uno ha tenido un enfoque y una interpretación únicos de las ideas gnósticas.
